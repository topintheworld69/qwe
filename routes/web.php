<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'reset' => false,
    'confirm' => false,
    'verify' => false,
    'register' => false,
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/', function () {
    return view('guest.index', ['data' => (new \App\Models\hospital)->index()]);
})->name('index');

Route::get('/find', function () {
    return view('guest.find');
})->name('find');

Route::get('/find/result', function (Request $request) {
    return view('guest.find', ['data' => (new \App\Models\hospital)->find($request->input('name'))]);
})->name('show');
Route::middleware(['auth'])->group(function () {
    Route::resource('admin/bolnoy', 'App\Http\Controllers\AdminController', ['parameters' => [
        'bolnoy' => 'id'
    ]]);
});
