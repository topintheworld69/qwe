<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use \App\Classes\Bolnoy;

class BolnoyTest extends TestCase
{

    public function testStub()
    {
        $stub = $this->getMockBuilder(Bolnoy::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->getMock();

        $stub->method('getDiagnoz')
            ->willReturn('orvi');

        $this->assertSame('orvi', $stub->getDiagnoz());
    }

    /** 2 = dva
     * @dataProvider palataDataProvider
     * @param $actual
     * @param $expected
     */

    public function testPalata($actual, $expected)
    {
        $result = $this->palata = $actual;
        $this->assertEquals($expected, $result);
    }

    public function palataDataProvider()
    {
        return array(
            array(18, 18),
            array(23, 23),
            array(1000, 1000)
        );
    }

}
