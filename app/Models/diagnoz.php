<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class diagnoz extends Model
{
    protected $table = "diagnoz";
    public function diagnozes(){
        return DB::table('diagnoz')->get();
    }
}
