<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class hospital extends Model
{
    protected $table = "hospital";

    public function index()
    {
        return DB::table('hospital')
            ->join('diagnoz as b', 'b.id', '=', 'hospital.bolezn')
            ->select('hospital.*', 'b.diagnoz as b_diagnoz')
            ->orderBy('hospital.id')
            ->get();

    }

    public function find($name)
    {
        return DB::table('hospital', 'h')
            ->join('diagnoz as b', 'b.id', '=', 'h.bolezn')
            ->select('h.*', 'b.diagnoz as b_diagnoz')
            ->where('h.l_name', $name)->get();
    }

    public function show($id)
    {
        return DB::table('hospital', 'h')
            ->join('diagnoz as b', 'b.id', '=', 'h.bolezn')
            ->select('h.*', 'b.diagnoz as b_diagnoz')
            ->where('h.id', $id)->get();
    }
}
