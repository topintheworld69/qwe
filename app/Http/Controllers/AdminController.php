<?php

namespace App\Http\Controllers;

use App\Models\hospital;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index', ['data' => (new \App\Models\hospital)->index()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.add', ['boleznes' => (new \App\Models\diagnoz)->diagnozes()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $add = new hospital();
        $add->l_name = $request->input('name');
        $add->date_hosp = $request->input('date');
        $add->palata_id = $request->input('palata');
        $add->bolezn = $request->input('diagnoz');
        $add->save();
        return redirect()->route('bolnoy.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.find', ['data' => (new \App\Models\hospital)->show($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.edit', ['result' => (new \App\Models\hospital)->show($id),
            'boleznes' => (new \App\Models\diagnoz)->diagnozes()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('hospital')
            ->where('id', $id)
            ->update([
                'l_name' => $request->input('name'),
                'date_hosp' => $request->input('date'),
                'palata_id' => $request->input('palata'),
                'bolezn' => $request->input('diagnoz')]);
        return redirect()->route('bolnoy.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('hospital')->where('id', '=', $id)->delete();
        return redirect()->route('bolnoy.index');
    }
}
