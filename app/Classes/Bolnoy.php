<?php


namespace App\Classes;


class Bolnoy
{
    public $diagnoz;
    public $name;
    public $palata;

    /**
     * @return mixed
     */
    public function getDiagnoz()
    {
        return $this->diagnoz;
    }

    /**
     * @param mixed $diagnoz
     */
    public function setDiagnoz($diagnoz): void
    {
        $this->diagnoz = $diagnoz;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPalata()
    {
        return $this->palata;
    }

    /**
     * @param mixed $palata
     */
    public function setPalata($palata): void
    {
        $this->palata = $palata;
    }

}
