@extends('admin.app.schema')
@section('title','Add')

@section('content')

    <h1>Введіть інформацію про хворобу</h1>
<form action="{{route('bolnoy.store')}}" method="post">
    @csrf
    <p>
        <label for="diagnoz">Назва хвороби</label>
        <select id="diagnoz" name="diagnoz" required>
            @foreach($boleznes as $bolezn)
                <option value='{{ $bolezn->id }}'>{{ $bolezn->diagnoz }}</option>
            @endforeach
        </select>
    </p>
    <p>
        <label for = "name">Iм`я хворого</label>
        <input type="text" placeholder="Введіть Iм`я" id="name" name="name" required>
    </p>
    <p>
        <label for="palata">Номер палати</label>
        <input type="text" placeholder="Введіть номер палати" id="palata_id" name="palata" required>
    </p>
    <p>
        <label for="diagnoz">Дата госпіталізації</label>
        <input type="date" placeholder="Введіть дату госпіталізації" id="date_hosp" name="date" required>
    </p>
    <p>
        <button type="submit">Додати</button>
    </p>
</form>
@endsection
