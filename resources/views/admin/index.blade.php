@extends('admin.app.schema')
@section('title','Main')

@section('content')
    <p>
{{--        <a href="{{route('bolnoy.show')}}">Пошук</a>\--}}
        <a href="{{route('bolnoy.create')}}">Додати</a>\
        <a class="dropdown-item" href="{{ route('logout') }}"
           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
    </p>
@endsection

