@extends('admin.app.schema')
@foreach($result as $res)
@section('title','Edit' .$res->l_name)

@section('content')

    <h1>Введіть інформацію про хворобу</h1>
    <form action="{{route('bolnoy.update', ['id' => $res->id] )}}" method="post">
        <input name="_method" type="hidden" value="PUT">
        @csrf
        <p>
            <label for="diagnoz">Назва хвороби</label>
            <select id="diagnoz" name="diagnoz" required>
                @foreach($boleznes as $bolezn)
                    <option value='{{ $bolezn->id }}'>{{ $bolezn->diagnoz }}</option>
                @endforeach
            </select>
        </p>

        <p>
            <label for = "name">Iм`я хворого</label>
            <input value="{{$res->l_name}}" type="text" placeholder="Введіть Iм`я" id="name" name="name" required>
        </p>

        <p>
            <label for="palata">Номер палати</label>
            <input value="{{$res->palata_id}}" type="text" placeholder="Введіть номер палати" id="palata_id" name="palata" required>
        </p>

        <p>
            <label for="diagnoz">Дата госпіталізації</label>
            <input value="{{$res->date_hosp}}" type="date" placeholder="Введіть дату госпіталізації" id="date_hosp" name="date" required>
        </p>

        <p>
            <button type="submit">Редагувати</button>
        </p>
    </form>
@endsection
@endforeach
